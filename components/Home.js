/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  TouchableHighlight, 
  Animated, Platform, StyleSheet,Alert } from 'react-native';
import { Icon,SearchBar } from 'react-native-elements'
import { Indicator, Pages } from 'react-native-pages';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { TouchableOpacity } from 'react-native-gesture-handler';
import Spinner from 'react-native-loading-spinner-overlay';
import FastImage from 'react-native-fast-image'
import {baseUrl} from './url'
const axios = require('axios');

let styles = StyleSheet.create({
    image: {
    width: '100%',
    height:100,
    resizeMode: 'cover',
    flex: 1,
    },

    container: {
    width: '100%',
    height:300,
    backgroundColor: '#263238',
    },

    textContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    },

    text: {
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 52,
    },

    index: {
    fontSize: 10,
    color: 'rgba(255, 255, 255, .63)',
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
      },
});

class Home extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'បញ្ជីអត្ថបទ',
        headerRight: (
            <SafeAreaView style={{marginRight :10}}>
                    <Icon onPress={()=> {
                        navigation.navigate('AddArticle',{isAdd : true})}
                        } name='add' />
            </SafeAreaView>
          )
      });
    constructor(props){
        super(props);
        AsyncStorage.removeItem('isView')
        this.state = {
            data : [],
            searchValue : '',
            loading : false,
            refreshing : false,
            isView : false,
            page : 1,
            url : 'https://cdn.the-scientist.com/assets/articleNo/66519/aImg/33885/rabbit-s.png',
            fetching_from_server : false,
        }
    }  
    fetchData = (title,isModify = false) => {
        let url = ''
        if(title){
            this.state.data = []
            this.state.page = 1
            url = `${baseUrl}/v1/api/articles?title=${title}&page=1&limit=20`
        }else{
            console.log(this.state.page)
            url = `${baseUrl}/v1/api/articles?page=${this.state.page}&limit=20`
        }
        axios({
            method : 'GET',
            url : url,
        }).then((res)=> {
            if(isModify==false){
                this.setState({
                    // page : this.state.page + 1,
                    data : [...this.state.data,...res.data.DATA],
                    refreshing : false,
                    fetching_from_server : false
                })
            }else{
                this.setState({
                    page : 1,
                    data : res.data.DATA,
                    refreshing : false,
                })
                
            }
            
        })
    }
    componentDidMount(){
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                AsyncStorage.getItem('isView').then((value)=>{
                    
                    if(value==null){
                        this.setState({
                            page : 1,
                        },()=>{
                            this.fetchData(this.state.searchValue,true)
                        })
                    }
                    if(value=='fasle'){
                        this.setState({
                            page : 1,
                        },()=>{
                            this.fetchData(this.state.searchValue,true)
                        })
                        this.listRef.getScrollResponder().scrollTo({x:0,animated:true})
                    }
                    
                })
                
                
                

                
            }
        );
    }
    viewDetail = (id) => {
        this.setState({
            isView : true
        })
        this.props.navigation.navigate('ViewDetail',{id:id})
    }
    onDelete = (data) => {
        Alert.alert('ចង់លុបរឺ?',
        data.TITLE,
        [
          {text: 'កែប្រែ', onPress: () => {this.setState({isView : false});this.props.navigation.navigate('AddArticle',{isAdd:false,id : data.ID})}},
          {
            text: 'អត់ទេ',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'យល់ព្រម', onPress: () => this.deleteArticle(data.ID)},
        ],
        {cancelable: false},)
    }
    deleteArticle = (id) => {
        this.setState({
            loading : true
        })
        axios({
            method : 'DELETE',
            url : `${baseUrl}/v1/api/articles/${id}`
        }).then((res)=>{
            this.setState({
                loading : false
            },()=>{
                Alert.alert('បានលុបដោយជោគជ័យ!!!')
                this.state.page = 1
                this.fetchData(null,true)
            })
        }).catch((res)=>{
            Alert.alert('បរាជ័យ!!!')
        })
    }
    CardArticle = (props) => {
        let data = props.data
        // let card = props.data.map((data) => {
                let year = data.CREATED_DATE.substr(0,4)
                let month = data.CREATED_DATE.substr(4,2)
                let day = data.CREATED_DATE.substr(6,2)
                return (
                    <TouchableOpacity key={"key"+data.ID}  onPress={()=>this.viewDetail(data.ID)} onLongPress={()=>this.onDelete(data)}>
                        <View key={data.ID}  style={{ flexDirection: "row",marginHorizontal:5,marginTop:5,backgroundColor:'white' }} >
                                <View style={{ width: 100, height: 100 }}>
                                    {
                                        data.IMAGE ? <Image onError = {()=>{
                                            data.IMAGE = this.state.url
                                        }} source={{uri:data.IMAGE}}  style={{width:100,height:100}} />
                                        : <Image source={{uri:data.IMAGE}}  source={{uri:this.state.url}} style={{width:100,height:100}} />
                                    }
                                    
                                </View>
                                <View
                                    style={{
                                            justifyContent: "space-between",
                                            marginLeft: 12,
                                            flex: 1
                                    }}>
                                    <View>
                                        <Text numberOfLines={2}>
                                            {data.TITLE.trim()}
                                        </Text>
                                        <View style={{flexDirection:'row'}}>
                                            <Icon style={{alignSelf:"center"}} name='alarm' />
                                            <Text style={{marginLeft:5,alignSelf:"center"}} numberOfLines={1}>{year+"-"+month+'-'+day}</Text>
                                        </View>
                                    
                                    </View>
                                </View>
                        </View>
                    </TouchableOpacity>
                    
                )
                
        // })
        return card
    }  
    updateSearch = (text) => {
        this.setState({
            ...this.state,
            searchValue : text
        })
        this.fetchData(text,true);
    }
    onRefresh = () => {
        this.setState({
            page : 1,
            refreshing : true
        },()=>{
            this.fetchData(null,true)
        })
    }
    handleLoadMore = () => {
        if(!this.state.fetching_from_server){
            this.state.fetching_from_server = true
            this.setState({
                page : this.state.page+1
            },()=>{
                this.fetchData(null,false)
            })
            
        }
        
    }
    renderFooter = () => {
        return (
          <View style={styles.footer}>
            {this.state.fetching_from_server ? (
              <ActivityIndicator color="black" style={{ margin: 15 }} />
            ) : null}
          </View>
        );
      }
      getHeader = () => {
        return (
                <View style={styles.container}>
                                <SearchBar
                                    round
                                    platform="default"
                                    placeholder="បញ្ចូលពាក្យស្វែងរក....."
                                    onChangeText={(text) => this.updateSearch(text)}
                                    value = {this.state.searchValue}
                                />
                                <Pages
                                    horizontal={true}
                                    indicatorPosition='bottom'
                                    indicatorColor='#FF9100'
                                    indicatorOpacity={0.5}
                                >
                                    <Image source={{uri:'https://www.flemishgiantrabbit.com/wp-content/uploads/2018/11/Best-Water-Bottle-Rabbits.jpg'}} style={styles.image} />
                                    <Image source={{uri:'https://amp.insider.com/images/5bf58205bde70f76925059c2-750-563.jpg'}} style={styles.image} />
                                    <Image source={{uri:'https://ichef.bbci.co.uk/images/ic/1280xn/p07ppctk.jpg'}} style={styles.image} />
                                </Pages>
                </View>
                )
    }  
    render(){
        return (
                <SafeAreaView style={{width:'100%',height:'100%',backgroundColor:'gray'}}>
                    <Spinner
                            visible={this.state.loading}
                            textContent={'កំពុងបញ្ជូន...'}
                            textStyle = {{color:'teal',fontSize:20,fontStyle:'italic'}}
                    />
                    
                    {
                        (this.state.data.length == 0 && this.state.searchValue != '') ?
                            <View style={{flex:1,flexDirection:'column',alignItems:'center',justifyContent: 'center'}}>
                                <Text style={{fontSize:30,color:"teal"}}>🔭រកមិនឃើញទេ!!🔍</Text>
                            </View>
                        
                        :
                        this.state.data.length != 0 ? 
                                <FlatList
                                    ListHeaderComponent={this.getHeader()}
                                    ref={(ref) => { this.listRef = ref; }}
                                    onRefresh={() => this.onRefresh()}
                                    refreshing = {this.state.refreshing}
                                    data={this.state.data}
                                    keyExtractor={(item, index) => "key"+index}
                                    onEndReached={()=>this.handleLoadMore()}
                                    onEndThreshold={0}
                                    ListFooterComponent = {()=>this.state.searchValue==''&&this.renderFooter()}
                                    renderItem={(data,index)=><this.CardArticle key={index} data={data.item}></this.CardArticle>}
                                ></FlatList>
                            
                        :
                            <SkeletonPlaceholder>
                                {[0,1,2,3,4,5].map((i)=>{
                                    return (
                                        <View key={i} style={{ flexDirection: "row",padding:10 }}>
                                            <View style={{ width: 100, height: 100 }} />
                                                <View
                                                style={{
                                                    justifyContent: "space-between",
                                                    marginLeft: 12,
                                                    flex: 1
                                                }}
                                                >
                                                    <View style={{ width: "100%", height: 20 }} />
                                                    <View style={{ width: "30%", height: 20 }} />
                                                    <View style={{ width: "80%", height: 20 }} />
                                                </View>
                                        </View>
                                    )
                                })} 
                            </SkeletonPlaceholder>
                    }
                    
                </SafeAreaView>
            
        )
    }
};

    
export default Home;
