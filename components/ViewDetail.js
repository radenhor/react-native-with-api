/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  BackHandler,
  AsyncStorage
} from 'react-native';
import { Icon,SearchBar } from 'react-native-elements'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Icon1 from 'react-native-vector-icons/MaterialIcons'
import {baseUrl} from './url'
const axios = require('axios');

class ViewDetail extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'លម្អិត'
    });
    constructor(props){
        super(props);
        this.state = {
            data : null
        }
    }
    componentDidMount(){
        AsyncStorage.setItem('isView','true').then(()=>{
            let id = this.props.navigation.getParam('id')
            axios({
                method : 'GET',
                url : `${baseUrl}/v1/api/articles/${id}`,
            }).then((res)=> {
                this.setState({
                    data : res.data.DATA
                })
            })
        })
        
    }
    componentWillUnmount(){
        this.props.navigation.navigate('Home',{isView:true})
    }
    Loading = () => {
        return (
            <SkeletonPlaceholder>
                <View style={{ flexDirection: "column"}}>
                    <View style={{ width: '100%', height: 200 }} />
                    <View style={{padding:10}}>
                        <View style={{ marginTop:10, width: "90%", height: 20 }} />
                        <View style={{ marginTop:10,width: "60%", height: 20 }} />
                        <View style={{marginTop:10 ,flexDirection:'row',justifyContent:'space-evenly'}}>
                            <View style={{ width: 20, height: 20 }} />
                            <View style={{ width: 20, height: 20 }} />
                            <View style={{ width: 20, height: 20 }} />
                        </View>
                        {
                            [...Array(100).keys()].map((i)=>{
                                return (<View key={i} style={{ marginTop:10, width: "100%", height: 20 }} />)
                            })
                        }
                    </View>
                </View>
            </SkeletonPlaceholder>
        )
    }
    Detail = (props) => {
                let year = props.data.CREATED_DATE.substr(0,4)
                let month = props.data.CREATED_DATE.substr(4,2)
                let day = props.data.CREATED_DATE.substr(6,2)
        return (
            <SafeAreaView style={{flex:1}}>
                <ScrollView>
                    <View style={{width:'100%',height:200}}>
                        {
                            props.data.IMAGE ?
                            <Image source={{uri:props.data.IMAGE}} style={{width: '100%',height:100,resizeMode: 'cover',flex: 1,}} />
                            :
                            <Image source={{uri:'https://cdn.the-scientist.com/assets/articleNo/66519/aImg/33885/rabbit-s.png'}} style={{width: '100%',height:100,resizeMode: 'cover',flex: 1,}} />
                        }
                        
                    </View>
                    <View style={{padding:10}}>
                        <Text numberOfLines={2} style={{fontSize:20,color:'teal',fontWeight:'bold'}}>{props.data.TITLE.trim()}</Text>
                        <View style={{flexDirection:'row'}}>
                            <Icon1 color={'teal'} size={20} style={{alignSelf:"center"}} name='date-range' />
                            <Text style={{marginLeft:5,alignSelf:"center",fontSize:15}} numberOfLines={1}>{year+"-"+month+'-'+day}</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <Icon name='bookmark' />
                            <Icon name='bookmark' />
                            <Icon name='bookmark' />
                        </View>
                        <Text style={{marginTop:20,fontSize:16}}>{props.data.DESCRIPTION.trim()}</Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
        
    }
    render(){
        return (
        <>
            <StatusBar barStyle="dark-content" />
            {
                this.state.data ? <this.Detail data = {this.state.data}></this.Detail> : <this.Loading></this.Loading>
            }
        </>
        );
  }
};



export default ViewDetail;
