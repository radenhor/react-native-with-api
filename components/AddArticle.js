/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  Button,
  Alert,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import {baseUrl} from './url'
// import RNFetchBlob from 'react-native-fetch-blob'
const axios = require('axios');
var io = require('socket.io-client');
// var ss = require('socket.io-stream');

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
class AddArticle extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'បន្ថែមអត្ថបទ'
    });
    constructor(props){
        super(props);
        this.state = {
            id : 0,
            title : '',
            desc : '',
            response : null,
            isAdd : true,
            url : 'https://cdn.pixabay.com/photo/2017/04/20/07/07/add-2244771_960_720.png',
            error : {
                title : '',
                desc : '',
                img : ''
            },
            heightDesc : 0,
            heightTitle : 0,
            loading : false
        }
    }
    componentDidMount(){
        let isAdd = this.props.navigation.getParam('isAdd',true)
        if(!isAdd){
            let id = this.props.navigation.getParam('id')
            axios({
                method : 'GET',
                url : `${baseUrl}/v1/api/articles/${id}`,
            }).then((res)=> {
                this.setState({
                    response : null,
                    id : id,
                    isAdd : false,
                    title : res.data.DATA.TITLE.trim(),
                    desc : res.data.DATA.DESCRIPTION.trim(),
                    url : res.data.DATA.IMAGE ? res.data.DATA.IMAGE : 'https://cdn.the-scientist.com/assets/articleNo/66519/aImg/33885/rabbit-s.png'
                })
            })
        }else{
            this.setState({
                id : 0,
                title : '',
                desc : '',
                response : null,
                isAdd : true,
                url : 'https://cdn.pixabay.com/photo/2017/04/20/07/07/add-2244771_960_720.png',
                error : {
                    title : '',
                    desc : '',
                    img : ''
                }
            })
        }
    }
    updateArticle = (id,image) => {
        let data = image ? {
            TITLE : this.state.title,
            DESCRIPTION : this.state.desc,
            IMAGE : image
        } : {
            TITLE : this.state.title,
            DESCRIPTION : this.state.desc,
            IMAGE : this.state.url

        }
        axios({
            method : 'PUT',
            url : `${baseUrl}/v1/api/articles/${id}`,
            data : data
        }).then((res) => {
            this.setState({
                loading : false
            })
            AsyncStorage.setItem('isView','fasle').then(()=>{
                this.props.navigation.navigate('Home',{isModify:true})
            })
            
        }).catch((res)=>{
            Alert.alert('កែប្រែបរាជ័យ!!')
        })
    }
    checkTitle = () => {
        if(this.state.title.trim()==''){
            this.setState({
                error : {
                    title : '*ចំណងជើងមិនអាចទទេ'
                }
            })
            return false
        }else{
            this.setState({
                error : {
                    title : ''
                }
            })
            return true
        }
    }
    checkDesc = () => {
        if(this.state.desc.trim()==''){
            this.setState({
                error : {
                    desc : '*បរិយាយមិនអាចទទេ'
                }
            })
            return false
        }else{
            this.setState({
                error : {
                    desc : ''
                }
            })
            return true
        }
    }
    checkImage = () => {
        if(this.state.response==null){
            this.setState({
                error:{
                    img : '*សូមបញ្ចូលរូបភាព'
                }
            })
            return false
        }else{
            this.setState({
                error:{
                    img : ''
                }
            })
            return true
        }
    }
    addArticle =  (image) => {
        axios({
            method:'POST',
            url : 'http://api-ams.me/v1/api/articles',
            data : {
                TITLE : this.state.title,
                DESCRIPTION : this.state.desc,
                IMAGE : image
            }
        }).then((res)=>{
            this.setState({
                loading : false
            })
            AsyncStorage.setItem('isView','fasle').then(()=>{
                this.props.navigation.navigate('Home',{isModify:true})
            })
        })
    }
    uploadImage = async () => {
        const formData = new FormData();
        formData.append('FILE', {
          name: this.state.response.fileName,
          type: this.state.response.type,
          uri:
            Platform.OS === 'android' ? this.state.response.uri : this.state.response.uri.replace('file://', ''),
        });
        return await axios({
          url:'http://api-ams.me/v1/api/uploadfile/single',
          method:'POST',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          data:formData
        })
    }
    onSubmit = () => {
        if(this.checkTitle()){
            if(this.checkDesc()){
                if(this.state.isAdd==true){
                    if(this.checkImage()){
                        this.setState({
                            loading : true
                        })
                        this.uploadImage().then((res)=>{
                            let image = res.data.DATA
                            this.addArticle(image)
                        }).catch((res)=>{
                           






                            Alert.alert('បញ្ហា',
                           'រូបភាពមិនត្រឹមត្រូវរឺទំហំធំពេល',
                            [
                              {text: 'យល់ព្រម', onPress: () => this.setState({loading:false})},
                            ],
                            {cancelable: false},)





                        })
                    }
                }else{
                    this.setState({
                        loading : true
                    })
                    if(this.state.response==null){
                        this.updateArticle(this.state.id,null)
                    }else{
                        this.uploadImage().then((res)=>{
                            let image = res.data.DATA
                            this.updateArticle(this.state.id,image)
                        })
                    }
                }
            }
        }
    }
    onPickImage = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
                this.setState({
                    ...this.state,
                    response : response,
                    url : response.uri
                })
            }
          });
    }
    render(){
        return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={{flex:1,flexDirection:'column',justifyContent:'center'}}>
                <ScrollView>
                <Spinner
                        visible={this.state.loading}
                        textContent={'កំពុងបញ្ជូន...'}
                        textStyle = {{color:'teal',fontSize:20,fontStyle:'italic'}}
                    />
                    <View>
                        <Image style={{width:120,height:100,alignSelf:'center'}} source={{uri:'https://images.vexels.com/media/users/3/151934/isolated/preview/9aa6fa876917b538540c5ea36f23c8e7-rabbit-animal-cartoon-by-vexels.png'}}></Image>
                        <Text style={{color:'teal',alignSelf:'center'}}>សូមស្វាគមន៏មកកាន់ទន្សាយApp</Text>
                        <View style={{marginTop:20,alignItems:'center',marginLeft:10,marginRight:10}}>
                            <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.title}</Text>
                            <KeyboardAvoidingView style={{width:'100%'}}>
                                <TextInput 
                                    multiline = {true}
                                    onContentSizeChange={(event) => {
                                        if(event.nativeEvent.contentSize.height<150){
                                            this.setState({ heightTitle: event.nativeEvent.contentSize.height })
                                        }
                                        
                                    }}
                                    style={{width:'100%',height: Math.max(70, this.state.heightTitle),backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} onChangeText={text=>this.setState({title:text})} value={this.state.title}  placeholder={'ចំណងជើង'}></TextInput>
                            </KeyboardAvoidingView>
                            <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.desc}</Text>
                            <KeyboardAvoidingView style={{width:'100%'}}>
                                <TextInput multiline={true} 
                                    onContentSizeChange={(event) => {
                                        if(event.nativeEvent.contentSize.height<360){
                                            this.setState({ heightDesc: event.nativeEvent.contentSize.height })
                                        }
                                        
                                    }}
                                    style={{width:'100%',height: Math.max(100, this.state.heightDesc),backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} onChangeText={text=>this.setState({desc:text})} value={this.state.desc} placeholder={'បរិយាយ'}></TextInput>
                            </KeyboardAvoidingView>
                            <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.img}</Text>
                            <TouchableOpacity onPress = {()=> this.onPickImage()}>
                                <Image  style={{marginTop:10,borderRadius:50,width:100,height:100,alignSelf:'center',marginBottom:10}} source={{uri:this.state.url}}></Image>
                            </TouchableOpacity>
                            
                            <Button onPress={()=>this.onSubmit()} style={{width:'100%',height:50,padding:5,marginTop:10,justifyContent:'center'}} title={this.state.isAdd ? 'បញ្ចូល' : 'រក្សាទុក'}></Button>
                        </View>
                    </View>
                </ScrollView>
                    
            </SafeAreaView>
        </>
        );
  }
};



export default AddArticle;
