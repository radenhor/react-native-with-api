/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {createAppContainer} from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import Home  from './components/Home'
import ViewDetail  from './components/ViewDetail'
import AddArticle  from './components/AddArticle'
const MainNavigator = createStackNavigator({
  Home: {screen: Home},
  ViewDetail: {screen: ViewDetail},
  AddArticle: {screen: AddArticle},
});

const App = createAppContainer(MainNavigator);

export default App;
